// list of modules to deploy
module "modulevpc" {
  name="${var.name}"
  numero="${var.numero}"
  source="./tfmodules/vpc"
  
  miregion="${var.regionaws}"
  miprofile="${var.profileaws}"

  sb_private_cidr="${var.subnet_private_cidr}"
  sb_public_cidr="${var.subnet_public_cidr}"
  sb_public2_cidr="${var.subnet_public2_cidr}"

  sb_private_az="${var.subnet_private_az}"
  sb_public_az="${var.subnet_public_az}"
  sb_public2_az="${var.subnet_public2_az}"

  r_cidr_block="${var.routing_cidr_block}"
}
