REPOSITORIO PUBLICO - TERRAFORM VPC - YAROSLAB
# Overview
this repository creates the following resources on AWS cloud.

# RESOURCES:
-   **+ Internal Gateway and NAT gateway:**

	aws_eip.eip_nat_gw:
            elastic ip to attach with internal NAT gateway

	aws_nat_gateway.my_ngw:
	    NAT Gateway to subnet private (for srv database,etc.)outbound any.

	aws_internet_gateway.my_igw:
            Internal Gateway to subnet public (ec2 with internet access) outbound all.

-   **+ Route Table inbound - outbound**

	aws_route_table.resource_rt_private:
	    kmkm
	
	aws_route_table.resource_rt_public:
	    mkmkm
	
	aws_route_table_association.rt_association_private:
	    resource to associate subnet private with route table private

	aws_route_table_association.rt_association_publics: (to subnet_public and subnet_public2)
 	    resource to associate subnet public with route table public

-   **+ SUBNETs**
	
	aws_subnet.resource_subnet_private:
	    subnet in network 192.168.1.0/24

	aws_subnet.resource_subnet_public:
	    subnet in network 192.168.2.0/24
        aws_subnet.resource_subnet_public2:
            subnet in network 192.168.3.0/24
	
-   **+ VPC**
	
	aws_vpc.vpc_main:
	    vpc with network 192.168.0.0/16	
		
