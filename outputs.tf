output "vpc_aws" {
  value = "${module.modulevpc.awsvpc}"
}
output "subnets_aws_private" {
  value = "${module.modulevpc.awssubnetsprivate}"
}
output "subnets_aws_public" {
  value = ["${module.modulevpc.awssubnetspublic}"]
}
output "subnets_aws_public2" {
  value = ["${module.modulevpc.awssubnetspublic2}"]
}

output "aws_nat_gateway" {
  value = "${module.modulevpc.awsnatgateway}"
}
output "aws_internet_gateway" {
  value = "${module.modulevpc.awsinternetgateway}"
}
