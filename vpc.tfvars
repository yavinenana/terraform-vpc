// ------------------vpc
name	= "tf.mivpc"
numero  = 1
regionaws   = "us-east-2"
profileaws  = "sysadmin-us-east-2"

subnet_private_cidr = "192.168.1.0/24"
subnet_public_cidr = "192.168.2.0/24"
subnet_public2_cidr = "192.168.3.0/24"

subnet_private_az = "us-east-2a"
subnet_public_az = "us-east-2b"
subnet_public2_az = "us-east-2c"

routing_cidr_block = "192.168.0.0/16"
