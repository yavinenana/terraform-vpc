// EIP TO NAT GATEWAY {private}
resource "aws_eip" "eip_nat_gw" {
  vpc = true
}
// NAT GATEWAY
resource "aws_nat_gateway" "my_ngw" {
  allocation_id = "${aws_eip.eip_nat_gw.id}"
  subnet_id     = "${aws_subnet.resource_subnet_public.id}"
  tags {
    Name        = "${var.name}-ngw"
  }
}
// ROUTE TABLE
/* Private */
resource "aws_route_table" "resource_rt_private" {
  vpc_id = "${aws_vpc.vpc_main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.my_ngw.id}"
  }
  tags {
    Name        = "${var.name}-rt-private"
    Env         = "${var.name}"
    Description = "Route table to private subnet ${var.name}"
  }
}
resource "aws_route_table_association" "rt_association_private" {
  subnet_id = "${aws_subnet.resource_subnet_private.id}"
  route_table_id = "${aws_route_table.resource_rt_private.id}"
}


// RESOURCE IGW
resource "aws_internet_gateway" "my_igw" {
  vpc_id = "${aws_vpc.vpc_main.id}"
  tags {
    Name        = "${var.name}-ig"
    Description = "Internet gateway"
  }
}
// ROUTE TABLE
/* Public */
resource "aws_route_table" "resource_rt_public" {
  vpc_id = "${aws_vpc.vpc_main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my_igw.id}"
  }  
  tags {
    Name        = "${var.name}-rt-public"
    Env         = "${var.name}"
    Description = "Route table to public subnet ${var.name}"
  }
}
resource "aws_route_table_association" "rt_association_publics" {
  subnet_id = "${aws_subnet.resource_subnet_public.id}"
  route_table_id = "${aws_route_table.resource_rt_public.id}"
}
resource "aws_route_table_association" "rt_association2_publics" {
  subnet_id = "${aws_subnet.resource_subnet2_public.id}"
  route_table_id = "${aws_route_table.resource_rt_public.id}"
}


/*resource "aws_nat_gateway" "my_ngw" {
  allocation_id = "${aws_eip.eip_nat_gw.id}"
  subnet_id     = "${aws_subnet.resource_subnet_public.id}"
  tags {
    Name        = "${var.name}-ngw"
  }
}
resource "aws_eip" "eip_nat_gw" {
  public_ip = "${var.gateway_public_ip}"
}*/
