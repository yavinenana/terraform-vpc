output "awsvpc" {
  value = "${aws_vpc.vpc_main.*.id}"
}
output "awssubnetsprivate" {
  value = "${aws_subnet.resource_subnet_private.*.id}"
}
output "awssubnetspublic" {
  value = ["${aws_subnet.resource_subnet_public.*.id}"]
}
output "awssubnetspublic2" {
  value = ["${aws_subnet.resource_subnet2_public.*.id}"]
}

output "awsnatgateway" {
  value = "${aws_nat_gateway.my_ngw.id}"
}
output "awsinternetgateway" {
  value = "${aws_internet_gateway.my_igw.id}"
}
