// RESOURCE AWS INSTANCE
resource "aws_vpc" "vpc_main" {
//  cidr_block="192.168.0.0/16"
  cidr_block="${var.r_cidr_block}"
  count         = "${var.numero}"
  instance_tenancy = "default"
  tags {
    Name = "vpc-${var.name}-${count.index+1}"
  }
}
