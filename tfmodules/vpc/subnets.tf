// RESOURCE SUBNET PRIVATE
resource "aws_subnet" "resource_subnet_private" {
  vpc_id     = "${aws_vpc.vpc_main.id}"
  cidr_block = "${var.sb_private_cidr}"
  availability_zone = "${var.sb_private_az}"
  tags {
    Name = "subnet-${var.name}-private"
  }
}
// RESOURCE SUBNET PUBLIC
resource "aws_subnet" "resource_subnet_public" {
  vpc_id     = "${aws_vpc.vpc_main.id}"
  cidr_block = "${var.sb_public_cidr}"
  availability_zone = "${var.sb_public_az}"
  tags {
    Name = "subnet-${var.name}-public"
  }
}

// RESOURCE SUBNET PUBLIC2
resource "aws_subnet" "resource_subnet2_public" {
  vpc_id     = "${aws_vpc.vpc_main.id}"
  cidr_block = "${var.sb_public2_cidr}"
  availability_zone = "${var.sb_public2_az}"
  tags {
    Name = "subnet-${var.name}-public2"
  }
}
