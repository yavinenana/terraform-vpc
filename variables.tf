variable "name" {}
variable "numero" {}
variable "regionaws" {}
variable "profileaws" {}

variable "subnet_private_cidr" {}
variable "subnet_public_cidr" {}
variable "subnet_public2_cidr" {}

variable "subnet_private_az" {}
variable "subnet_public_az" {}
variable "subnet_public2_az" {}

variable "routing_cidr_block" {}
